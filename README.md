# BMC / Remote Control packages

These are utilities I have used to do remote control of embedded systems, especially for bare metal CI/CD testing.

* mTerm - a terminal multiplexer
   This allows multiple clients to access a serial tty at the same time.
   This utility comes from Facebook's [openBMC](https://github.com/facebook/openbmc/tree/helium/common/recipes-core/mTerm).

   The OpenWrt config and init script will spawn a telnet server on the specified port.

* Stub [Redfish](https://www.dmtf.org/standards/redfish) server
   All this does at the moment is implement the API call for reboot.

   A GPIO (in Linux /sys/class/gpio) is required for the target reset action.

These utilities are designed to be used in closed environments and no effort has been made to implement access control.

You can see an example of this in action with GitLab CI in the 
[Traverse Distribution Images project](https://gitlab.com/traversetech/ls1088firmware/distribution-images) - look
under [test/ten64](https://gitlab.com/traversetech/ls1088firmware/distribution-images/-/tree/master/test/ten64) for a Python script.